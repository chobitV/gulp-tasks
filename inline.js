const gulp = require('gulp');
const replace = require('gulp-replace');
const fs = require("fs");
const sync = require('browser-sync').get('sync');

const config = require('../config');


gulp.task('inline', function(){
	return gulp.src([config.src.production + '*.html'])
    .pipe(replace('<link href="css/inline.css" rel="stylesheet">', function(){
    	let css = fs.readFileSync(config.src.css + 'inline.css', "utf8");
    	return '<style>\n' + css + '\n</style>'
    }))
    .pipe(gulp.dest(config.src.production))
    .pipe(sync.stream());
});

gulp.task('inline:watch', function() {
    gulp.watch(config.src.scss + 'inline.scss', ['inline']);
});
